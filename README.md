Hi! Welcome to the example project. We hope it helps you learn Git and GitLab!

The README.md file is the **face of your project**. It helps people get to know some general things about your project like: *what and why are you even doing?*, *what technologies are you using?*, *how to install your project?*, *what does it depend on?* and finally you should include some examples of what it can do!

.md files have a specific ruleset ou must follow (you can open this file in a code editor to see a sample)

**Remember to include visuals!! They are much more compelling and most importantly you can show off just how cool your project is!!**
Also, remember to add a **License** - without it people can't freely use your work for developing other important projects:((( 

Below you can find an example of a README.md

### code4change website (c4c.org.pl)
## Created for you
[![Generic badge](https://img.shields.io/badge/Powered%20by-code4change-orange.svg)](https://c4c.org.pl/)
![](photos/website.png)
